﻿using System.ComponentModel.DataAnnotations;

namespace Seldat
{
    public class ClothCategoryModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
