﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CategoryApiController : ApiController
    {
        ClothCategoryLogic logic = new ClothCategoryLogic();
        [HttpGet]
        [Route("api/getCategories")]
        public HttpResponseMessage GetCategories()
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetErrors());
                List<ClothCategoryModel> categories = logic.GetCategories();
                return Request.CreateResponse(HttpStatusCode.Created, categories);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                logic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
