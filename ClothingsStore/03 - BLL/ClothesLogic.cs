﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat
{
    public class ClothesLogic : BaseLogic
    {
        public List<ClothModel> GetClothByCategory(int id)
        {
           return DB.Clothes.Include("Categories").Include("Types").Include("Companies")
                  .Where(p => p.ClothCategory == id)
                  .Select(p => new ClothModel
                  {
                      id = p.ClothID,
                      category = new ClothCategoryModel { id = p.ClothCategory, name = p.Category.CategoryName },
                      type = new ClothTypeModel { id = p.ClothType, descripition = p.Type.TypeName },
                      company = new ClothCompanyModel { id = p.ClothCompany, name = p.Company.CompanyName },
                      price = (double)p.Price,
                      discount = p.Discount,
                      picture = p.Picture
                  }).ToList();
        }

        public ClothModel GetOneCloth(int id)
        {
            return DB.Clothes.Where(c => c.ClothID == id).Select(c => new ClothModel
            {
                id = c.ClothID,
                category = new ClothCategoryModel { id = c.ClothCategory, name = c.Category.CategoryName },
                type = new ClothTypeModel { id = c.ClothType, descripition = c.Type.TypeName },
                company = new ClothCompanyModel { id = c.ClothCompany, name = c.Company.CompanyName },
                price = (double)c.Price,
                discount = c.Discount,
                picture = c.Picture
            }).FirstOrDefault();
        }

        public ClothModel AddCloth(ClothModel model)
        {
            Cloth cloth = new Cloth
            {
                ClothCategory = model.category.id,
                ClothCompany = model.company.id,
                ClothType = model.type.id,
                Price = (decimal)model.price,
                Discount = model.discount,
            };
            DB.Clothes.Add(cloth);
            DB.SaveChanges();
            model.id = cloth.ClothID;
            return model;
        }

        public void UploadFile(int id, string picture)
        {
            Cloth cloth = new Cloth();
            cloth = DB.Clothes.FirstOrDefault(c => c.ClothID == id);
            cloth.Picture = picture;
            DB.SaveChanges();
        }

        public ClothModel UpdateFullCloth(ClothModel model)
        {
            Cloth cloth = new Cloth { ClothID = model.id };
            DB.Clothes.Attach(cloth);
            cloth.ClothCategory = model.category.id;
            cloth.ClothCompany = model.company.id;
            cloth.ClothType = model.type.id;
            cloth.Price = (decimal)model.price;
            cloth.Discount = model.discount;
            cloth.Picture = model.picture;
            DB.SaveChanges();
            return model;
        }

        public void UpdateFile(int id, string picture)
        {
            Cloth cloth = new Cloth();
            cloth = DB.Clothes.FirstOrDefault(c => c.ClothID == id);
            cloth.Picture = picture;
            DB.SaveChanges();
        }

        public string GetFileName(int id)
        {
            var cloth = DB.Clothes.Where(c => c.ClothID == id).FirstOrDefault();
            return cloth?.Picture;
        }

        public void DeleteCloth(int id)
        {
            Cloth cloth = new Cloth();
            cloth = DB.Clothes.FirstOrDefault(c => c.ClothID == id);
            DB.Clothes.Remove(cloth);
            DB.SaveChanges();
        }

    }
}
