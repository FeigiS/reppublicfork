﻿using System;

namespace Seldat
{
    public class BaseLogic : IDisposable
    {
        int x = 3;
        double y = 2;
        protected ClothesEntities DB = new ClothesEntities();
        public void Dispose()
        {
            DB.Dispose();
        }
    }
}
